<?php

require_once  $_SERVER['DOCUMENT_ROOT'] . "/helpers/Config.php";

#Vinculando a dependência do Model
require_once  $_SERVER['DOCUMENT_ROOT'] . "/models/Pagina.php";

#Função responsável por listar as páginas na view index.php
function index()
{
   
}

#Função responsável por visualizar os dados da páginas na view visualizar.php
function visualizar($id)
{
    
}

#Função responsável por cadastrar os dados das páginas na view cadastrar.php
function cadastrar()
{
    
}

#Função responsável por editar os dados das páginas na view editar.php
function editar($id)
{
   
}

#Função responsável por deletar o registro o página
function deletar($id)
{
    
}